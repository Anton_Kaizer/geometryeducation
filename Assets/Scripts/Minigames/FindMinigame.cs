using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;


namespace GeometryEducation.Minigames
{
    internal sealed class FindMinigame : BaseMinigame<FindMinigameContainer, FindMinigameDescription>
    {
        private FindMinigameTaskDescription _task;
        private int _rightCounter;
        
        public FindMinigame(FindMinigameContainer container, FindMinigameDescription description, IMainMenuController menuController) : base(container, description, menuController)
        {
        }
        
        protected override void OnStart()
        {
            var index = Random.Range(0, _description.Tasks.Count);
            _task = _description.Tasks.Values.ToList()[index];
            SetupFiguresColorsAndTask();
            
            InitItems();
        }

        private void CompleteGame()
        {
            _container.CompleteGame(_description);
        }

        private void SetupFiguresColorsAndTask()
        {
            _container.SetColors(_description);
            foreach (var figure in _description.Figures.Keys)
            {
                _container.SetColorForType(figure);
            }

            _container.SetupTask(this, _task);
        }

        private void InitItems()
        {
            var types = _description.Figures.Keys.ToList();
            IDictionary<string, int> itemTypes = types.ToDictionary(type => type, type => 0);

            foreach (var sceneItem in _container.Items)
            {
                InitItem(types, itemTypes, sceneItem);
            }

            _rightCounter = itemTypes[_task.FigureType];
        }

        private void InitItem(IList<string> types, IDictionary<string, int> itemTypes, FindMinigameSceneItem sceneItem)
        {
            var type = types[Random.Range(0, types.Count)];
            while (itemTypes[type] >= _description.MaxSameItems)
            {
                type = types[Random.Range(0, types.Count)];
            }

            sceneItem.Init(type, _description.Figures[type][Random.Range(0, _description.Figures[type].Count)]);
            _container.SetItemColor(sceneItem);
            itemTypes[type]++;
        }

        private void OnSceneItemItemClicked(FindMinigameSceneItem item)
        {
            if (item.Id == _task.FigureType)
            {
                RightAnswer(item);
            }
            else
            {
                _container.ProceedWrongAnswer(this, _description);
            }
        }
        
        private void OnRestartButtonClicked()
        {
            _container.CompletedPanel.SetSafeActive(false);
            Stop();
        }

        private void RightAnswer(FindMinigameSceneItem item)
        {
            _rightCounter--;
            
            item.SetUnavailable();
            
            if (_rightCounter <= 0)
                CompleteGame();
            else
                _container.ProceedRightAnswer(this, _description);
        }

        protected override void OnStop()
        {
            if (_container == null) return;
            
            _container.ClearColorsAndTask();
            _rightCounter = 0;
            _task = null;
            foreach (var sceneItem in _container.Items)
            {
                sceneItem.Dispose();
            }
        }

        protected override void OnAttach()
        {
            foreach (var sceneItem in _container.Items)
            {
                sceneItem.ItemClicked += OnSceneItemItemClicked;
            }
            _container.RestartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        protected override void OnDetach()
        {
            foreach (var sceneItem in _container.Items)
            {
                sceneItem.ItemClicked += OnSceneItemItemClicked;
            }
            _container.RestartButton.onClick.RemoveListener(OnRestartButtonClicked);
        }
    }
}