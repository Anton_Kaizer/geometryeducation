using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;

namespace GeometryEducation.Minigames
{
    internal abstract class BaseMinigame<TContainer, TDescription> : IMinigame where TContainer : MinigameContainer where TDescription : BaseMinigameDescription
    {
        public GameObject Root => _container.Root;

        protected readonly TContainer _container;
        protected readonly TDescription _description;

        protected readonly IMainMenuController _mainMenuController;
        protected bool _isStarted;
        
        protected BaseMinigame(TContainer container, TDescription description, IMainMenuController mainMenuController)
        {
            _container = container;
            _description = description;
            _mainMenuController = mainMenuController;
            
            SetupView();
            Attach();
        }

        private void SetupView()
        {
            SetGamePanelActive(false);
            
            _container.DescriptionText.text = _description.DescriptionText;
            _container.TitleText.text = _description.TitleText;
        }

        public void Start()
        {
            if(_isStarted) return;
            _isStarted = true;
            Random.InitState(Time.frameCount);

            SetGamePanelActive(true);

            OnStart();
        }

        public void Stop()
        {
            if(!_isStarted) return;
            _isStarted = false;

            SetGamePanelActive(false);

            OnStop(); 
        }

        public void Dispose()
        {
            Stop();
            Detach();
        }
        
        protected void SetGamePanelActive(bool isActive)
        {
            if (_container == null) return;
            
            _container.IntroducePanel.SetSafeActive(!isActive);
            _container.GamePanel.SetSafeActive(isActive);
        }

        private void Attach()
        {
            _container.StartButton.onClick.AddListener(Start);
            foreach (var returnToMainManButton in _container.ReturnToMainMenuButtons)
            {
                returnToMainManButton.onClick.AddListener(_mainMenuController.ReturnToMainPage);
            }

            OnAttach();
        }

        private void Detach()
        {
            _container.StartButton.onClick.RemoveListener(Start);
            foreach (var returnToMainManButton in _container.ReturnToMainMenuButtons)
            {
                returnToMainManButton.onClick.RemoveListener(_mainMenuController.ReturnToMainPage);
            }

            OnDetach();
        }

        protected abstract void OnStart();
        protected abstract void OnStop();
        protected virtual void OnAttach() {}
        protected virtual void OnDetach() {}
    }
}