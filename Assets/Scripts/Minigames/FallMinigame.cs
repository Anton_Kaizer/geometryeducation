using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.MonobehView;
using GeometryEducation.References;
using UnityEngine;
using Random = UnityEngine.Random;


namespace GeometryEducation.Minigames
{
    internal sealed class FallMinigame : SelectableMinigame<FallMinigameContainer, FallMinigameDescription>, IUpdatable, ICountedMinigame
    {
        public int CurCounter => _completeCounter;
        
        private FallMinigameTaskDescription _task;

        private readonly IList<string> _figureIds;
        
        private int _completeCounter;
        private float _lastSpawnTime;
        private readonly IList<FallMinigameSceneItem> _items = new List<FallMinigameSceneItem>();

        public FallMinigame(FallMinigameContainer container, FallMinigameDescription description, IMainMenuController menuController) : base(container, description, menuController)
        {
            _figureIds = _description.Figures.Keys.ToList();
        }
        
        protected override void OnStart()
        {
            _container.CompletedPanel.SetSafeActive(false);
            
            _task = _description.Tasks[_selection.Item.Id];
            _completeCounter = _task.Count;
            
            SetupFiguresColorsAndTask();
            
            SpawnAndColorRandomFigure();
            
            _container.UserBasket.Init("user_basket", _container.GamePanel.transform as RectTransform, _mainMenuController.Container.Camera);
        }

        public void Update(float dt)
        {
            if(!_isStarted) return;
            
            if (Time.time - _lastSpawnTime > _description.SpawnFrequency)
            {
                SpawnAndColorRandomFigure();
            }
        }

        private void ProceedRightAnswer()
        {
            _completeCounter--;
            
            if (_completeCounter == 0)
                CompleteGame();
            else 
                _container.ProceedRightAnswer(this, _description);
        }

        private void CompleteGame()
        {
            _container.IntroducePanel.SetSafeActive(false);
            _container.GamePanel.SetSafeActive(false);
            _container.CompletedPanel.SetSafeActive(true);
            
            _container.CompleteGame(_description);
            
            Stop();
        }

        private void ProceedWrongAnswer()
        {
            _container.ProceedWrongAnswer(this, _description);
        }

        private void SetupFiguresColorsAndTask()
        {
            _container.SetColors(_description);
            foreach (var figure in _figureIds)
            {
                _container.SetColorForType(figure);
            }

            _container.SetupTask(this, _task);
        }

        private void SpawnAndColorRandomFigure()
        {
            var figureId = _figureIds[Random.Range(0, _description.Figures.Count)];
            var item = Object.Instantiate(_container.FigurePrefab, _container.GameBoardRoot.transform, false);
            item.Init(figureId, _description.FiguresSpeed, _description.FiguresSize, _description.Figures[figureId][Random.Range(0, _description.Figures[figureId].Count)]);
            
            _container.SetImageColor(item.Image, figureId);
            item.RectTransform.PlaceObjectAtARandomHighPointOnPanel(_container.GamePanel.transform as RectTransform);

            _items.Add(item);
            AttachItem(item);
            _lastSpawnTime = Time.time;
        }

        private void OnItemCollidedExit(FallMinigameSceneItem item)
        {
            if(item == null) return;
            
            DetachItem(item);
            Object.Destroy(item.gameObject);
        }

        private void OnItemCollidedBasket(FallMinigameSceneItem item)
        {
            if (_task.FigureType == item.Id)
                ProceedRightAnswer();
            else
                ProceedWrongAnswer();

            OnItemCollidedExit(item);
        }

        private void OnRestartButtonClick()
        {
            _container.CompletedPanel.SetSafeActive(false);
            SetGamePanelActive(false);
        }
        
        protected override void OnStop()
        {
            _container.ClearColorsAndTask();
            _container.UserBasket.Dispose();
            foreach (var sceneItem in _items)
            {
                OnItemCollidedExit(sceneItem);
            }
            _items.Clear();
        }
        
        private void AttachItem(FallMinigameSceneItem item)
        {
            item.ItemCollidedBasket += OnItemCollidedBasket;
            item.ItemCollidedExit += OnItemCollidedExit;
        }

        private void DetachItem(FallMinigameSceneItem item)
        {
            item.ItemCollidedBasket -= OnItemCollidedBasket;
            item.ItemCollidedExit -= OnItemCollidedExit;
        }

        protected override void OnAttach()
        {
            base.OnAttach();
            
            _container.RestartButton.onClick.AddListener(OnRestartButtonClick);
        }

        protected override void OnDetach()
        {
            base.OnDetach();
            
            _container.RestartButton.onClick.AddListener(OnRestartButtonClick);
        }
    }
}