using System.Collections.Generic;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace GeometryEducation.Minigames
{
    internal abstract class SelectableMinigame<TContainer, TDescription> : BaseMinigame<TContainer, TDescription> where TContainer : SelectableMinigameContainer where TDescription : SelectableMinigameDescription
    {
        private readonly IList<PreviewSelectionItem> _selectionItems = new List<PreviewSelectionItem>();
        
        protected readonly Selection<PreviewSelectionItem> _selection = new Selection<PreviewSelectionItem>();
       
        protected SelectableMinigame(TContainer container, TDescription description, IMainMenuController mainMenuController) : base(container, description, mainMenuController)
        {
            SetupSelectionGrid();
            
            _selection.Item = _selectionItems[0];
            UpdateSelectionView();
        }

        private void UpdateSelectionView()
        {
            foreach (var item in _selectionItems)
            {
                if(item == _selection.Item)
                    SetSelectedView(item);
                else 
                    SetDefaultView(item);
            }
        }

        private void SetupSelectionGrid()
        {
            foreach (var selectionDescription in _description.SelectionDescriptions)
            {
                var item = Object.Instantiate(_container.PreviewSelectionItemPrefab, _container.SelectionPanelRoot, false);
                Addressables.LoadAssetAsync<Sprite>(selectionDescription.PreviewTexture).Completed += operation => item.PreviewImage.sprite = operation.Result;
                item.Init(selectionDescription.Task);
                AttachItem(item);
                _selectionItems.Add(item);
            }
        }
        
        private void OnItemSelected(Selection<PreviewSelectionItem> self, PreviewSelectionItem oldValue, PreviewSelectionItem newValue)
        {
            if(newValue == oldValue) return;
            
            UpdateSelectionView();
        }

        private void SetSelectedView(PreviewSelectionItem item)
        {
            if (item == null) return;
            
            item.BackgroundImage.color = _description.SelectedItemColor;
            item.transform.localScale = new Vector3(_description.SelectedItemScale, _description.SelectedItemScale, _description.SelectedItemScale);
        }

        private void SetDefaultView(PreviewSelectionItem item)
        {
            if (item == null) return;
            
            item.BackgroundImage.color = _description.DefaultItemColor;
            item.transform.localScale = Vector3.one;
        }

        private void OnItemClicked(PreviewSelectionItem item)
        {
            _selection.Item = item;
        }
        
        private void AttachItem(PreviewSelectionItem item)
        {
            item.SelfButtonClicked += OnItemClicked;
        }
        
        private void DetachItem(PreviewSelectionItem item)
        {
            item.SelfButtonClicked -= OnItemClicked;
        }
        
        protected override void OnAttach()
        {
            _selection.ItemSelected += OnItemSelected;
        }
        
        protected override void OnDetach()
        {
            foreach (var item in _selectionItems)
            {
                DetachItem(item);
                item.Dispose();
            }

            _selection.ItemSelected -= OnItemSelected;
        }
    }
}