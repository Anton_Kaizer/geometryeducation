using System;

namespace GeometryEducation.Minigames
{
    internal sealed class Selection<T>
    {
        public event Action<Selection<T>, T, T> ItemSelected; 
        public T Item
        {
            get => _item;
            set
            {
                var oldValue = _item;
                _item = value;
                
                ItemSelected?.Invoke(this, value, oldValue);
            }
        }

        private T _item;
    }
}