using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


namespace GeometryEducation.Minigames
{
    internal sealed class BuildMinigame : SelectableMinigame<BuildMinigameContainer, BuildMinigameDescription>
    {
        private BuildMinigameTaskDescription _currentTask;
        private IBuildMinigameItem _currentItem = new EmptyBuildMinigameItem();

        public BuildMinigame(BuildMinigameContainer container, BuildMinigameDescription description, IMainMenuController menuController) : base(container, description, menuController)
        {
        }

        protected override void OnStart()
        {
            _currentTask = _description.Tasks[_selection.Item.Id];
            _container.SetupTask(this, _currentTask);
            SpawnItem();
        }

        private void SpawnItem()
        {
            Addressables.LoadAssetAsync<GameObject>(_currentTask.PrefabId).Completed += OnItemLoaded;
        }

        private void OnItemLoaded(AsyncOperationHandle<GameObject> operation)
        {
            operation.Result.TryGetComponent<BuildMinigameItemContainer>(out var itemContainer);

            _currentItem = Object.Instantiate(itemContainer, _container.GameBoardRoot, false);
            _currentItem.Init("line", _mainMenuController.Container.Camera, _container.GameBoardRoot);
        }
        
        private void OnCheckButtonPressed()
        {
            if(!_isStarted || _currentItem is EmptyBuildMinigameItem) return;

            if (_currentItem.CheckItemCorrect())
                CompleteGame();
            else
                _container.ProceedWrongAnswer(this, _description);
        }

        private void CompleteGame()
        {
            _container.CompleteGame(_description);
        }
        
        private void OnRestartButtonPressed()
        {
            Stop();
        }

        protected override void OnStop()
        {
            _currentTask = null;
            if (_currentItem is BuildMinigameItemContainer container && container != null)
            {
                Object.Destroy(_currentItem.GameObject);
            }
        }

        protected override void OnAttach()
        {
            base.OnAttach();
            
            _container.RestartButton.onClick.AddListener(OnRestartButtonPressed);
            _container.CheckButton.onClick.AddListener(OnCheckButtonPressed);
        }

        protected override void OnDetach()
        {
            base.OnDetach();
            
            _container.RestartButton.onClick.RemoveListener(OnRestartButtonPressed);
            _container.CheckButton.onClick.RemoveListener(OnCheckButtonPressed);
        }
    }
}