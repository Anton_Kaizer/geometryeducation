using UnityEngine;

namespace GeometryEducation.Extensions
{
    internal static class SizeCalculator
    {
        public static void PlaceObjectAtARandomHighPointOnPanel(this RectTransform self, RectTransform panel)
        {
            var position = self.position;
            self.position = new Vector3(position.x, position.y, 1.0f);
            var itemSize = self.sizeDelta.x;
            var rect = panel.rect;
            var xOffset = rect.width - 1280.0f;

            var panelMinX = rect.min.x + itemSize / 2 + 30;
            var panelMaxX = rect.max.x - itemSize / 2 - xOffset - 30;

            var positionX = Random.Range(panelMinX, panelMaxX);
            self.localPosition = new Vector3(positionX, itemSize / 2, 0.0f);
        }

        public static void PlaceObjectAtARandomPointOnPanel(this RectTransform self, RectTransform panel)
        {
            var position = self.position;
            self.position = new Vector3(position.x, position.y, 1.0f);
            var itemSizeX = self.sizeDelta.x;
            var itemSizeY = self.sizeDelta.y;
            
            var rect = panel.rect;

            var xOffset = rect.width - 1280.0f;
            var yOffset = rect.height - 550.0f;
            
            var panelMinX = rect.min.x + itemSizeX * 0.5f + 30;
            var panelMaxX = rect.max.x - itemSizeX  * 0.5f - xOffset - 30;
            var panelMaxY = rect.max.y - itemSizeY * 0.5f - 50;
            var panelMinY = rect.min.y + itemSizeY * 0.5f + + yOffset + 50;

            var positionX = Random.Range(panelMinX, panelMaxX);
            var positionY = Random.Range(panelMinY, panelMaxY);
            self.localPosition = new Vector3(positionX, positionY, 0.0f);
        }
    }
}