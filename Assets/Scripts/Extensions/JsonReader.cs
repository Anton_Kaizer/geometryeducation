using fastJSON;
using GeometryEducation.Interfaces;

namespace GeometryEducation
{
    class JsonReader : IJsonReader
    {
        public T ReadFromJson<T>(string jsonText)
        {
            return jsonText == string.Empty ? default : (T)JSON.Parse(jsonText);
        }
    }
}