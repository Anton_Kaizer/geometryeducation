using UnityEngine;

namespace GeometryEducation.Extensions
{
    public static class ArrayExtensions
    {
        public static T TryGetValue<T>(this object[] objectArray, int index, T defaultValue = default)
        {
            if (objectArray == null || index >= objectArray.Length)
                return default;

            return objectArray[index] is T tValue ? tValue : defaultValue;
        } 
        
        public static void SetSafeActive(this GameObject go, bool active)
        {
            if (go.activeSelf != active)
                go.SetActive(active);
        }

    }
}