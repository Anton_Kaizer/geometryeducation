using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.Minigames;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;


namespace GeometryEducation
{
    internal sealed class MainMenuController : IMainMenuController
    {
        public MainMenuContainer Container => _container;
        
        private IMinigame _currentMinigame;
        private readonly IUpdateManager _updateManager;
        
        private readonly MainMenuContainer _container;
        private readonly MainMenuDescription _description;
        
        public MainMenuController(MainMenuContainer container, MainMenuDescription description, IUpdateManager updateManager)
        {
            _container = container;
            _description = description;
            _updateManager = updateManager;
            
            _container.TheoreticalPage.SetupTheoreticalPage(_description.TheoreticalPageDescription);
            _container.MinigamesPage.SetupMinigamesPage(_description.MinigamesPageDescription);

            Attach();
        }
        
        public void ReturnToMainPage()
        {
            StopCurrentMinigame();
            
            _container.MenuPanel.SetSafeActive(true);
            
            _container.SetTitleText(_description.TitleText);
            _container.MainButtonsPanel.gameObject.SetSafeActive(true);
            
            _container.MinigamesPage.Root.SetSafeActive(false);
            _container.TheoreticalPage.Root.SetSafeActive(false);
        }

        private void OnTheoreticalButtonClick()
        {
            _container.MainButtonsPanel.gameObject.SetSafeActive(false);
            _container.SetTitleText(_description.TheoreticalPageDescription.TitleText);
            
            _container.TheoreticalPage.ShowTheoreticalPage();
        }
        
        private void OnMinigamesButtonClick()
        {
            _container.MainButtonsPanel.gameObject.SetSafeActive(false);
            _container.SetTitleText(_description.MinigamesPageDescription.TitleText);
            
            _container.MinigamesPage.ShowMinigamesPage();
        }
        
        private void StopCurrentMinigame()
        {
            if(_currentMinigame == null) return;
            
            if(_currentMinigame is IUpdatable updatable) _updateManager.Remove(updatable);
           
            _currentMinigame.Dispose();
            
            Object.Destroy(_currentMinigame.Root);
            _currentMinigame = null;
        }

        private void OnFirstMinigameButtonClick()
        {
            StopCurrentMinigame();        
            _container.MenuPanel.SetSafeActive(false);
            
            _currentMinigame = new FallMinigame(Object.Instantiate(_container.FallMinigameContainer, _container.Root.transform), _description.FallMinigameDescription, this);
            if(_currentMinigame is IUpdatable updatable) _updateManager.Add(updatable);
        }

        private void OnSecondMinigameButtonClick()
        {
            StopCurrentMinigame();            
            _container.MenuPanel.SetSafeActive(false);

            _currentMinigame = new BuildMinigame(Object.Instantiate(_container.BuildMinigameContainer, _container.Root.transform), _description.BuildMinigameDescription, this);
            if(_currentMinigame is IUpdatable updatable) _updateManager.Add(updatable);
        }
        
        private void OnThirdMinigameButtonClick()
        {
            StopCurrentMinigame();
            _container.MenuPanel.SetSafeActive(false);

            _currentMinigame = new FindMinigame(Object.Instantiate(_container.FindMinigameContainer, _container.Root.transform), _description.FindMinigameDescription, this);
            if(_currentMinigame is IUpdatable updatable) _updateManager.Add(updatable);
        }
        
        private static void OnExitButtonClick()
        {
            Application.Quit();
        }
        
        private void Attach()
        {
            _container.MinigamesPage.FirstGameButton.onClick.AddListener(OnFirstMinigameButtonClick);
            _container.MinigamesPage.SecondGameButton.onClick.AddListener(OnSecondMinigameButtonClick);
            _container.MinigamesPage.ThirdGameButton.onClick.AddListener(OnThirdMinigameButtonClick);
            
            _container.TheoreticalPage.ClosePage.onClick.AddListener(ReturnToMainPage);
            _container.MinigamesPage.ClosePage.onClick.AddListener(ReturnToMainPage);

            _container.MinigamesButton.onClick.AddListener(OnMinigamesButtonClick);
            _container.TheoreticalButton.onClick.AddListener(OnTheoreticalButtonClick);
            _container.ExitButton.onClick.AddListener(OnExitButtonClick);
        }

        private void Detach()
        {
            _container.MinigamesPage.FirstGameButton.onClick.RemoveListener(OnFirstMinigameButtonClick);
            _container.MinigamesPage.SecondGameButton.onClick.RemoveListener(OnSecondMinigameButtonClick);
            _container.MinigamesPage.ThirdGameButton.onClick.RemoveListener(OnThirdMinigameButtonClick);
            
            _container.TheoreticalPage.ClosePage.onClick.RemoveListener(ReturnToMainPage);
            _container.MinigamesPage.ClosePage.onClick.RemoveListener(ReturnToMainPage);

            _container.MinigamesButton.onClick.RemoveListener(OnMinigamesButtonClick);
            _container.TheoreticalButton.onClick.RemoveListener(OnTheoreticalButtonClick);
            _container.ExitButton.onClick.RemoveListener(OnExitButtonClick);
        }

        public void Dispose()
        {
            StopCurrentMinigame();
            Detach();
        }
    }
}