using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class MainMenuDescription
    {
        public TheoreticalPageDescription TheoreticalPageDescription { get; }
        public MinigamesPageDescription MinigamesPageDescription { get; }
        
        public BuildMinigameDescription BuildMinigameDescription { get; }
        public FallMinigameDescription FallMinigameDescription { get; }
        public FindMinigameDescription FindMinigameDescription { get; }

        public string TitleText { get; }
        
        public MainMenuDescription(IDictionary<string, object> initNode)
        {
            TheoreticalPageDescription = new TheoreticalPageDescription(initNode.TryGetNode("theoretical_page"));
            MinigamesPageDescription = new MinigamesPageDescription(initNode.TryGetNode("minigames_page"));

            var minigamesNode = initNode.TryGetNode("minigames"); 
            BuildMinigameDescription = new BuildMinigameDescription(minigamesNode.TryGetNode("build"));
            FallMinigameDescription = new FallMinigameDescription(minigamesNode.TryGetNode("fall"));
            FindMinigameDescription = new FindMinigameDescription(minigamesNode.TryGetNode("find"));

            TitleText = initNode.GetString("title_text");
        }
    }
}