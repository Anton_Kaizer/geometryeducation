using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public class TheoreticalPageDescription
    {
        public IList<(string, string, string)> Items { get; } = new List<(string, string, string)>(16);
     
        public string TitleText { get; }
        
        public TheoreticalPageDescription(IDictionary<string, object> initNode)
        {
            TitleText = initNode.GetString("title_text");
            foreach (var infoItem in initNode.GetObjectList("items"))
            {
                var data = infoItem as IList<object>;
                Items.Add((data.GetString(0), data.GetString(1), data.GetString(2)));
            }
        }
    }
}