using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class MinigamesPageDescription
    {
        public string TitleText { get; }
        public string FirstMinigameName { get; }
        public string SecondMinigameName { get; }
        public string ThirdMinigameName { get; }
        
        public MinigamesPageDescription(IDictionary<string, object> initNode)
        {
            FirstMinigameName = initNode.GetString("first_minigame_name");
            SecondMinigameName = initNode.GetString("second_minigame_name");
            ThirdMinigameName = initNode.GetString("third_minigame_name");
            TitleText = initNode.GetString("title_text");
        }
    }
}