using System;
using System.Collections.Generic;
using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace GeometryEducation.References
{
    internal interface IReferences
    {
        bool IsLoaded { get; }
        event Action LoadComplete;
        MainMenuDescription MenuDescription { get; }  
    }
    
    internal sealed class References : IReferences
    {
        public event Action LoadComplete;
        public bool IsLoaded { get; private set; }
        public MainMenuDescription MenuDescription { get; private set; }

        private readonly IJsonReader _jsonReader;
        
        public References(IJsonReader jsonReader)
        {
            _jsonReader = jsonReader;
            Addressables.LoadAssetAsync<TextAsset>("references_References").Completed += OnReferencesLoaded;
        }

        private void OnReferencesLoaded(AsyncOperationHandle<TextAsset> operation)
        {
            var initNode = _jsonReader.ReadFromJson<IDictionary<string, object>>(operation.Result.text);

            MenuDescription = new MainMenuDescription(initNode.TryGetNode("main_menu"));
            IsLoaded = true;
            LoadComplete?.Invoke();
        }
    }
}