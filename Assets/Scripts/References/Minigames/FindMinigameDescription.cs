using System.Collections.Generic;
using GeometryEducation.Extensions;
using UnityEngine;

namespace GeometryEducation.References
{
    public sealed class FindMinigameDescription : BaseMinigameDescription
    {
        public IDictionary<string, FindMinigameTaskDescription> Tasks { get; } = new Dictionary<string, FindMinigameTaskDescription>();
        public IDictionary<string, IList<string>> Figures { get; } = new Dictionary<string, IList<string>>();
        
        public int MaxSameItems { get; }
        public HashSet<Color> Colors { get; }

        public FindMinigameDescription(IDictionary<string, object> initNode) : base(initNode)
        {
            var tasksNode = initNode.TryGetNode("tasks");
            foreach (var taskId in tasksNode.Keys)
            {
                Tasks.Add(taskId, new FindMinigameTaskDescription(tasksNode.TryGetNode(taskId)));    
            }
            var figuresNode = initNode.TryGetNode("figures");
            foreach (var figureId in figuresNode.Keys)
            {
                Figures.Add(figureId, figuresNode.GetStringList(figureId));
            }
            Colors = new HashSet<Color>(initNode.GetObjectList("colors").GetColorList());

            MaxSameItems = initNode.GetInt("max_same_items", 8);
        }
    }
}