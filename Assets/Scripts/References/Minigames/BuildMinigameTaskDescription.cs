using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class BuildMinigameTaskDescription
    {
        public string PreviewTextureId { get; }
        public string PrefabId { get; }
        public string Description { get; }

        public BuildMinigameTaskDescription(IDictionary<string, object> initNode)
        {
            PreviewTextureId = initNode.GetString("preview_texture");
            PrefabId = initNode.GetString("prefab_id");
            Description = initNode.GetString("description");
        }
    }
}