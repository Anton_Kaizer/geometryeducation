using System.Collections.Generic;
using GeometryEducation.Extensions;
using UnityEngine;

namespace GeometryEducation.References
{
    public abstract class SelectableMinigameDescription : BaseMinigameDescription
    {
        public IList<SelectionDescription> SelectionDescriptions { get; } = new List<SelectionDescription>();
        public HashSet<Color> Colors { get; }
        public Color SelectedItemColor { get; }
        public Color DefaultItemColor { get; }
        public float SelectedItemScale { get; }

        protected SelectableMinigameDescription(IDictionary<string, object> initNode) : base(initNode)
        {
            foreach (var selectionNode in initNode.GetNodeList("selections"))
            {
                SelectionDescriptions.Add(new SelectionDescription(selectionNode));
            }

            Colors = new HashSet<Color>(initNode.GetObjectList("colors").GetColorList());
            SelectedItemColor = initNode.GetColor("selected_item_color");
            DefaultItemColor = initNode.GetColor("default_item_color");
            SelectedItemScale = initNode.GetFloat("selected_item_scale", 1.1f);
        }
    }
}