using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class FallMinigameDescription : SelectableMinigameDescription
    {
        public IDictionary<string, FallMinigameTaskDescription> Tasks  { get; } = new Dictionary<string, FallMinigameTaskDescription>();
        public IDictionary<string, IList<string>> Figures { get; } = new Dictionary<string, IList<string>>();

        public float FiguresSpeed { get; }
        public float SpawnFrequency { get; }
        public float FiguresSize { get; }
        
        public FallMinigameDescription(IDictionary<string, object> initNode) : base(initNode)
        {
            var taskNodes = initNode.TryGetNode("tasks");
            foreach (var taskNodeId in taskNodes.Keys)
            {
                Tasks.Add(taskNodeId, new FallMinigameTaskDescription(taskNodes.TryGetNode(taskNodeId)));    
            }

            var figuresNode = initNode.TryGetNode("figures");
            foreach (var figureId in figuresNode.Keys)
            {
                Figures.Add(figureId, figuresNode.GetStringList(figureId));
            }

            SpawnFrequency = initNode.GetFloat("spawn_frequency");
            FiguresSize = initNode.GetFloat("figures_size");
            FiguresSpeed = initNode.GetFloat("figures_fall_speed");
        }
    }
}