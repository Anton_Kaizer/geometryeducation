using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class FallMinigameTaskDescription
    {
        public string CollectText { get; }
        public string FigureTypeText { get; }
        public string FigureType { get; }
        public int Count { get; }

        public FallMinigameTaskDescription(IDictionary<string, object> initNode)
        {
            CollectText = initNode.GetString("collect_text");
            FigureTypeText = initNode.GetString("figure_type_text");
                
            FigureType = initNode.GetString("figure_type");
            Count = initNode.GetInt("count");
        }
    }
}