using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class BuildMinigameDescription : SelectableMinigameDescription
    {
        public IDictionary<string, BuildMinigameTaskDescription> Tasks { get; } = new Dictionary<string, BuildMinigameTaskDescription>();
        public float WrongPanelTime { get; }
        
        public BuildMinigameDescription(IDictionary<string, object> initNode) : base(initNode)
        {
            var taskNodes = initNode.TryGetNode("tasks");
            foreach (var taskNodeId in taskNodes.Keys)
            {
                Tasks.Add(taskNodeId, new BuildMinigameTaskDescription(taskNodes.TryGetNode(taskNodeId)));    
            }
            WrongPanelTime = initNode.GetFloat("wrong_panel_time");
        }
    }
}