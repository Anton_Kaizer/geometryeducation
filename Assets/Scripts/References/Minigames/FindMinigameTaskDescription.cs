using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class FindMinigameTaskDescription
    {
        public string DescriptionText { get; }
        public string FigureType { get; }

        public FindMinigameTaskDescription(IDictionary<string, object> initNode)
        {
            FigureType = initNode.GetString("figure_type");
            DescriptionText = initNode.GetString("description");
        }
    }
}