using System.Collections.Generic;
using GeometryEducation.Extensions;

namespace GeometryEducation.References
{
    public sealed class SelectionDescription
    {       
        public string Task { get; }
        public string PreviewTexture { get; }

        public SelectionDescription(IDictionary<string, object> initNode)
        {
            Task = initNode.GetString("task");
            PreviewTexture = initNode.GetString("preview_texture");
        }
    }
}