using System.Collections.Generic;
using GeometryEducation.Extensions;
using UnityEngine;

namespace GeometryEducation.References
{
    public abstract class BaseMinigameDescription
    {
        public string TitleText { get; }
        public string DescriptionText { get; }
        public string RightAnswerText { get; }
        public Color RightAnswerColor { get; }
        public Color CompleteColor { get; }
        public string CompleteText { get; }
        public string WrongAnswerText { get; }
        public Color WrongAnswerColor { get; }
        public float WrongRightPanelTime { get; }

        protected BaseMinigameDescription(IDictionary<string, object> initNode)
        {
            TitleText = initNode.GetString("title_text");
            DescriptionText = initNode.GetString("description");
            
            WrongAnswerColor = initNode.GetColor("wrong_color");
            WrongAnswerText = initNode.GetString("wrong_text");
            RightAnswerColor = initNode.GetColor("right_color");
            RightAnswerText = initNode.GetString("right_text");
            CompleteText = initNode.GetString("complete_text");
            
            WrongRightPanelTime = initNode.GetFloat("wrong_right_panel_time", 1.0f);
            CompleteColor = initNode.GetColor("complete_color");
        }
    }
}