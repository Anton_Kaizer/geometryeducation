using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class MainMenuContainer : MonoBehaviour
    {
        [Space] 
        public GameObject Root;
        public GameObject MenuPanel;
        public Camera Camera;

        public GameObject MainButtonsPanel;
        
        [Space]
        public TextMeshProUGUI TitleText;

        [Space] 
        public FindMinigameContainer FindMinigameContainer;
        public BuildMinigameContainer BuildMinigameContainer;
        public FallMinigameContainer FallMinigameContainer;
        
        [Space]
        public Button TheoreticalButton;
        public Button MinigamesButton;
        public Button ExitButton;
        
        [Space]
        public TheoreticalPageContainer TheoreticalPage;
        public MinigamesPageContainer MinigamesPage;

        public void SetTitleText(string text)
        {
            TitleText.text = text;
        }
    }
}