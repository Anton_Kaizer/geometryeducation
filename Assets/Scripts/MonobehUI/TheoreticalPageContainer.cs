using System.Collections.Generic;
using GeometryEducation.Extensions;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class TheoreticalPageContainer : MonoBehaviour
    {
        public GameObject Root;
        
        [Space]
        public Button ClosePage;
        
        [Space]
        [SerializeField] private RectTransform _scrollContent;
        
        [Space]
        [SerializeField] private TheoreticalItemContainer _itemPrefab;

        private readonly IList<TheoreticalItemContainer> _items = new List<TheoreticalItemContainer>();
        
        public void SetupTheoreticalPage(TheoreticalPageDescription description)
        {
            Root.SetSafeActive(true);
                        
            SetContentSize(description);
           
            SetupItems(description);

            Root.SetSafeActive(false);
        }
        
        public void ShowTheoreticalPage()
        {
            Root.SetSafeActive(true);
        }

        private void SetupItems(TheoreticalPageDescription description)
        {
            var index = 0;
            foreach (var (spriteKey, nameText, descriptionText) in description.Items)
            {
                _items[index].Root.SetSafeActive(true);
                Addressables.LoadAssetAsync<Sprite>(spriteKey).Completed += _items[index].SetSpriteOnLoad;
                _items[index].DescriptionText.text = descriptionText;
                _items[index].NameText.text = nameText;

                if (spriteKey == "textures_Dot") _items[index].Image.transform.localScale = new Vector3(0.2f, 0.2f, 0.5f);
                
                index++;
            }
        }
        
        private void SetContentSize(TheoreticalPageDescription description)
        {
            for (var i = 0; i < description.Items.Count; i++)
            {
                _items.Add(Instantiate(_itemPrefab, _scrollContent));
            }

            _scrollContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, description.Items.Count * 300 + 80);
        }
    }
}