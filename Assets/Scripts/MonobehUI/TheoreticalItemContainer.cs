using TMPro;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal class TheoreticalItemContainer : MonoBehaviour
    {
        public GameObject Root; 
        public TextMeshProUGUI DescriptionText;
        public TextMeshProUGUI NameText;
        public Image Image;

        public void SetSpriteOnLoad(AsyncOperationHandle<Sprite> operation)
        {
            Image.sprite = operation.Result;
        }
    }
}