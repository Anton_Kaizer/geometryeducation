using System.Collections;
using System.Collections.Generic;
using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.References;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal abstract class MinigameContainer : MonoBehaviour
    {
        public RectTransform GameBoardRoot => _gameBoardRoot;
        public GameObject CompletedPanel => _completedPanel;
        
        public GameObject Root;
        public GameObject IntroducePanel;
        public GameObject GamePanel;
        [SerializeField] private RectTransform _gameBoardRoot;

        [Space]
        public List<Button> ReturnToMainMenuButtons;
        public Button StartButton;

        [Space]
        public TextMeshProUGUI DescriptionText;
        public TextMeshProUGUI TitleText;
        
        [Space] 
        [SerializeField] protected Image _wrongRightPanel; 
        [SerializeField] protected GameObject _completedPanel; 
        [SerializeField] protected TextMeshProUGUI _taskDescriptionText; 
        [SerializeField] protected TextMeshProUGUI _completedText;

        public void ProceedRightAnswer(IMinigame minigame, BaseMinigameDescription description)
        {
            StopAllCoroutines();

            _wrongRightPanel.color = description.RightAnswerColor;
            _taskDescriptionText.text = description.RightAnswerText;
            
            StartCoroutine(ResetToDefault(minigame, description));
        }

        public void ProceedWrongAnswer(IMinigame minigame, BaseMinigameDescription description)
        {
            StopAllCoroutines();

            _wrongRightPanel.color = description.WrongAnswerColor;
            _taskDescriptionText.text = description.WrongAnswerText;
            
            StartCoroutine(ResetToDefault(minigame, description));
        }

        public void CompleteGame(BaseMinigameDescription description)
        {
            StopAllCoroutines();
            _completedPanel.SetSafeActive(true);
            _wrongRightPanel.raycastTarget = true;
            _taskDescriptionText.text = string.Empty;
            
            _wrongRightPanel.color = description.CompleteColor;
            _completedText.text = description.CompleteText;
            OnComplete();
        }

        protected void SetDefaultView(IMinigame minigame)
        {
            _completedPanel.SetSafeActive(false);
            _completedText.text = string.Empty;
            _wrongRightPanel.color = Color.clear;
            _wrongRightPanel.raycastTarget = false;
            SetDefaultTaskView(minigame);
        }
        
        private IEnumerator ResetToDefault(IMinigame minigame, BaseMinigameDescription description)
        {
            yield return new WaitForSeconds(description.WrongRightPanelTime);
            
            SetDefaultView(minigame);
        }

        protected abstract void SetDefaultTaskView(IMinigame minigame);

        protected virtual void OnComplete() {}
    }
}