using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Interfaces;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GeometryEducation.MonobehUI
{
    internal sealed class FindMinigameContainer : MinigameContainer
    {
        public List<FindMinigameSceneItem> Items => _items;
        public Button RestartButton => _restartButton;
        
        [Space]
        [SerializeField] private List<FindMinigameSceneItem> _items;

        [Space] 
        [SerializeField] private Button _restartButton;
        
        private readonly IDictionary<string, Color> _typeColors = new Dictionary<string, Color>();
        private readonly HashSet<Color> _usedColors = new HashSet<Color>();
        private HashSet<Color> _colors = new HashSet<Color>();

        private FindMinigameTaskDescription _task;

        public void SetItemColor(FindMinigameSceneItem sceneItem)
        {
            sceneItem.ImageView.color = _typeColors[sceneItem.Id];
        }
        
        public void SetColorForType(string figureType)
        {
            var unusedColors = new HashSet<Color>(_colors);
            unusedColors.ExceptWith(_usedColors);
            var color = unusedColors.ToList()[Random.Range(0, unusedColors.Count)];

            _typeColors[figureType] = color;
            _usedColors.Add(color);
        }
        
        public void ClearColorsAndTask()
        {
            _typeColors.Clear();
            _usedColors.Clear();
            if (this != null) StopAllCoroutines();
            _task = null;
        }

        protected override void SetDefaultTaskView(IMinigame minigame)
        {
            _taskDescriptionText.text = _task.DescriptionText;
        }

        public void SetupTask(IMinigame minigame, FindMinigameTaskDescription task)
        {
            _task = task;

            SetDefaultView(minigame);
        }

        public void SetColors(FindMinigameDescription description)
        {
            _colors = description.Colors;
        }
    }
}