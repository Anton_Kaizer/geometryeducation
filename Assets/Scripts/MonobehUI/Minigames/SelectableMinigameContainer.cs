using UnityEngine;

namespace GeometryEducation.MonobehUI
{
    internal abstract class SelectableMinigameContainer : MinigameContainer
    {
        public PreviewSelectionItem PreviewSelectionItemPrefab => previewSelectionItemPrefab;
        public RectTransform SelectionPanelRoot => _selectionPanelRoot;
        [Space]   
        [SerializeField] private PreviewSelectionItem previewSelectionItemPrefab;
        [SerializeField] private RectTransform _selectionPanelRoot;
    }
}