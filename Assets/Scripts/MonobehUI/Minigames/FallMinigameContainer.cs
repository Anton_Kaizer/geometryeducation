using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehView;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GeometryEducation.MonobehUI
{
    internal sealed class FallMinigameContainer : SelectableMinigameContainer
    {
        [Space] 
        public FallMinigameSceneItem FigurePrefab;
        
        [Space] 
        public UserBasketSceneItem UserBasket;
        
        [Space] 
        public Button RestartButton; 
        
        private readonly IDictionary<string, Color> _typeColors = new Dictionary<string, Color>();
        private readonly HashSet<Color> _usedColors = new HashSet<Color>();
        private HashSet<Color> _colors = new HashSet<Color>();

        private FallMinigameTaskDescription _task;

        public void SetImageColor(Image image, string type)
        {
            image.color = _typeColors[type];
        }
        
        public void SetColorForType(string figureType)
        {
            var unusedColors = new HashSet<Color>(_colors);
            unusedColors.ExceptWith(_usedColors);
            var color = unusedColors.ToList()[Random.Range(0, unusedColors.Count)];

            _typeColors[figureType] = color;
            _usedColors.Add(color);
        }
        
        public void ClearColorsAndTask()
        {
            _typeColors.Clear();
            _usedColors.Clear();
            if(this != null) StopAllCoroutines();
            _task = null;
        }

        protected override void SetDefaultTaskView(IMinigame minigame)
        {
            var curCounter = ((ICountedMinigame) minigame).CurCounter;
            _taskDescriptionText.text = $"{_task.CollectText} {curCounter} {_task.FigureTypeText}{GetNameEnding(_task.FigureType != "circle", curCounter % 20)}";
        }

        private static string GetNameEnding(bool isFigure, int meaningCounter)
        {
            return meaningCounter switch
            {
                0 => GetEndings(isFigure).Item2,
                1 => GetEmptyEnding(isFigure),
                _ => meaningCounter < 5 ? GetEndings(isFigure).Item1 : GetEndings(isFigure).Item2
            };
        }

        private static string GetEmptyEnding(bool isFigure)
        {
            return isFigure ? string.Empty : "ь";
        }

        private static (string, string) GetEndings(bool isFigure)
        {
            return isFigure ? ("а", "ов") : ("и", "ей");
        }

        public void SetupTask(IMinigame minigame, FallMinigameTaskDescription task)
        {
            _task = task;
            
            SetDefaultView(minigame);
        }

        public void SetColors(SelectableMinigameDescription description)
        {
            _colors = description.Colors;
        }
    }
}