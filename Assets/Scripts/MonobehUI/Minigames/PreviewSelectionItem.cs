using System;
using GeometryEducation.MonobehView;
using UnityEngine;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class PreviewSelectionItem : BaseSceneItem, IDisposable
    {
        public event Action<PreviewSelectionItem> SelfButtonClicked;
        public Image BackgroundImage => _backgroundImage;
        public Image PreviewImage => _previewImage;
        
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _previewImage;
        [SerializeField] private Button _selfButton;
        
        protected override void OnInit(params object[] initalParams)
        {
            _selfButton.onClick.AddListener(OnSelfButtonClicked);
        }
        
        private void OnSelfButtonClicked()
        {
            SelfButtonClicked?.Invoke(this);
        }

        private void OnDestroy()
        {
            Dispose();
        }

        public void Dispose()
        {
            _selfButton.onClick.RemoveListener(OnSelfButtonClicked);
        }
    }
}