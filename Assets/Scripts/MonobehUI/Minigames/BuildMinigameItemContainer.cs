using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Interfaces;
using GeometryEducation.MonobehView;
using UnityEngine;

namespace GeometryEducation.MonobehUI
{
    internal sealed class BuildMinigameItemContainer : MonoBehaviour, IBuildMinigameItem
    {
        [SerializeField] private List<BuildMinigameLineView> _lines;

        public GameObject GameObject => gameObject ? gameObject : null;

        public bool CheckItemCorrect()
        {
            return _lines.All(line => line.CheckLineComplete());
        }

        public void Init(params object[] initalParams)
        {
            foreach (var line in _lines)
            {
                line.Init(initalParams);
            }
        }
    }

    internal sealed class EmptyBuildMinigameItem : IBuildMinigameItem
    {
        public GameObject GameObject => null;
        public bool CheckItemCorrect() => false;
        public void Init(params object[] initalParams) { }
    }

    internal interface IBuildMinigameItem : IInitable
    {
        GameObject GameObject { get; }
        bool CheckItemCorrect();
    }
}