using GeometryEducation.Extensions;
using GeometryEducation.Interfaces;
using GeometryEducation.References;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class BuildMinigameContainer : SelectableMinigameContainer
    {
        public Button CheckButton => _checkButton;
        public Button RestartButton => _restartButton;
        
        [Space]
        [SerializeField] private Image _taskImage;

        [Space] 
        [SerializeField] private Button _checkButton;

        [Space] 
        [SerializeField] private GameObject _gameButtonsPanel;
        [SerializeField] private Button _restartButton;


        public void SetupTask(IMinigame minigame, BuildMinigameTaskDescription taskDescription)
        {
            Addressables.LoadAssetAsync<Sprite>(taskDescription.PreviewTextureId).Completed += operation => _taskImage.sprite = operation.Result;
            _taskDescriptionText.text = taskDescription.Description;
            
            SetDefaultView(minigame);
        }

        protected override void OnComplete()
        {
            _gameButtonsPanel.SetSafeActive(false);
        }

        protected override void SetDefaultTaskView(IMinigame minigame)
        {
            _gameButtonsPanel.SetSafeActive(true);
        }
    }
}