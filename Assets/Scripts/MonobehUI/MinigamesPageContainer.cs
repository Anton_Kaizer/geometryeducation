using GeometryEducation.Extensions;
using GeometryEducation.References;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class MinigamesPageContainer : MonoBehaviour
    {
        public GameObject Root;
        
        [Space]
        public Button ClosePage;
        
        [Space]
        public Button FirstGameButton;
        public Button SecondGameButton;
        public Button ThirdGameButton;

        public void SetupMinigamesPage(MinigamesPageDescription description)
        { 
            Root.SetSafeActive(true);
            
            FirstGameButton.GetComponentInChildren<TextMeshProUGUI>().text = description.FirstMinigameName;
            SecondGameButton.GetComponentInChildren<TextMeshProUGUI>().text = description.SecondMinigameName;
            ThirdGameButton.GetComponentInChildren<TextMeshProUGUI>().text = description.ThirdMinigameName;
            
            Root.SetSafeActive(false);
        }
        
        public void ShowMinigamesPage()
        {
            Root.SetSafeActive(true);
        }
    }
}