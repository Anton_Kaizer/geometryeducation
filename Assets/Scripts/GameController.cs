using GeometryEducation.Interfaces;
using GeometryEducation.MonobehUI;
using GeometryEducation.References;
using UnityEngine;

namespace GeometryEducation
{
    public sealed class GameController : MonoBehaviour
    {
        [SerializeField] private MainMenuContainer _mainMenuContainer;
        
        private IReferences _references;
        private IMainMenuController _menuController;
        private IUpdateManager _updateManager;
        private bool _inited;

        private void Awake()
        {
            DontDestroyOnLoad(this);

            _references = new References.References(new JsonReader());
            if (_references.IsLoaded)
                ReferencesOnLoadComplete();
            else
                _references.LoadComplete += ReferencesOnLoadComplete;
        }

        private void ReferencesOnLoadComplete()
        {
            _updateManager = new UpdateManager();

            _menuController = new MainMenuController(_mainMenuContainer, _references.MenuDescription, _updateManager);
            _menuController.ReturnToMainPage();
            _inited = true;
        }

        private void Update()
        {
            if (_inited)
            {
                _updateManager.Update(Time.deltaTime);  
            }
        }
        
        private void OnDestroy()
        {
            _inited = false;
            _menuController.Dispose();
        }
    }
}

