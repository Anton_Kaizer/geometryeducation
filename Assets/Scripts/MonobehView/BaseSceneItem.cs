using System;
using GeometryEducation.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GeometryEducation.MonobehView
{
    public abstract class BaseSceneItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IIdentified, IInitable
    {
        public string Id { get; private set; }

        public event Action<BaseSceneItem, PointerEventData> PointerDowned; 
        public event Action<BaseSceneItem, PointerEventData> PointerUpped;

        public RectTransform RectTransform { get; private set; }

        public void Init(params object[] initalParams)
        {
            if(initalParams == null) return;
            RectTransform = (RectTransform) transform;
            
            Id = Convert.ToString(initalParams[0]); 
            
            OnInit(initalParams);
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            PointerDowned?.Invoke(this, eventData);
            
            OnPointerDowned(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            PointerUpped?.Invoke(this, eventData);
            
            OnPointerUpped(eventData);
        }

        protected abstract void OnInit(params object[] initalParams);
        protected virtual void OnPointerDowned(PointerEventData eventData) {}
        protected virtual void OnPointerUpped(PointerEventData eventData) {}
    }
}