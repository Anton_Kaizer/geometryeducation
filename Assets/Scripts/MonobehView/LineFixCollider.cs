using UnityEngine;

namespace GeometryEducation.MonobehView
{
    [RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
    internal sealed class LineFixCollider : MonoBehaviour
    {
        public bool CorrectIntersection { get; private set; }

        [SerializeField] private int _fixId;

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.TryGetComponent<LineFixCollider>(out var fixCollider))
            {
                CorrectIntersection = fixCollider._fixId == _fixId;
            }
        }
        
        private void OnCollisionExit2D(Collision2D other)
        {
            CorrectIntersection = false;
        }
    }
}