using System;
using GeometryEducation.Extensions;
using GeometryEducation.MonobehView;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace GeometryEducation.MonobehUI
{
    internal sealed class FindMinigameSceneItem : BaseSceneItem, IDisposable
    {
        public event Action<FindMinigameSceneItem> ItemClicked;
        public Image ImageView => _imageView;
        
        [SerializeField] private Button _selfButton;
        [SerializeField] private Image _imageView;
        private Image _backgroundView;

        private readonly Color _backgroundColor = new Color(195.0f/255.0f, 195.0f/255.0f, 195.0f/255.0f, 145.0f/255.0f); 
        
        protected override void OnInit(params object[] initalParams)
        {
            _backgroundView = GetComponent<Image>();
            _backgroundView.color = _backgroundColor;
            
            _selfButton.interactable = true;
            _selfButton.onClick.AddListener(OnSelfButtonClicked);

            Addressables.LoadAssetAsync<Sprite>(initalParams.GetString(1)).Completed += operation => 
                _imageView.sprite = operation.Result;
        }

        public void SetUnavailable()
        {
            _selfButton.interactable = false;
            _imageView.color = Color.clear;
            _backgroundView.color = Color.clear;
        }

        private void OnSelfButtonClicked()
        {
            ItemClicked?.Invoke(this);
        }

        public void Dispose()
        {
            _selfButton.onClick.RemoveListener(OnSelfButtonClicked);
        }
    }
}