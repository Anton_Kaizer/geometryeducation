using System.Collections.Generic;
using System.Linq;
using GeometryEducation.Extensions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GeometryEducation.MonobehView
{
    public sealed class BuildMinigameLineView : BaseSceneItem
    {
        [SerializeField] private List<LineFixCollider> _fixPoints;
        
        private bool _isSelected;
        private Camera _camera;
        private Vector3 _catchPosition;
        private RectTransform _gameBoardTransform;
        private bool _isStopped;

        protected override void OnInit(params object[] initalParams)
        {
            _camera = (Camera) initalParams[1];
            _gameBoardTransform = (RectTransform) initalParams[2];
            RectTransform.PlaceObjectAtARandomPointOnPanel(_gameBoardTransform);
        }

        public bool CheckLineComplete()
        {
            return _fixPoints.All(fixPoint => fixPoint.CorrectIntersection);
        }
        
        protected override void OnPointerDowned(PointerEventData eventData)
        {
            _isSelected = true;
            
            _catchPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        }
        
        protected override void OnPointerUpped(PointerEventData eventData)
        {
            _isSelected = false;
        }

        private void Update()
        { 
            if(!_isSelected) return;
            
            MoveLineToPoint(_camera.ScreenToWorldPoint(Input.mousePosition));
        }

        private void MoveLineToPoint(Vector3 screenToWorldPoint)
        {
            var offset = screenToWorldPoint - _catchPosition;
            if(Mathf.Approximately(offset.magnitude, 0.0f) || !CheckBounds(screenToWorldPoint)) return;
            
            _catchPosition = screenToWorldPoint; 
            RectTransform.position += new Vector3(offset.x, offset.y, 0);
        }
        
        private bool CheckBounds(Vector3 screenToWorldPoint)
        {
            var innerPoint = RectTransform.parent.InverseTransformPoint(screenToWorldPoint);
            var boardRect = _gameBoardTransform.rect;
            var rect = RectTransform.rect;
            var halfWidth = rect.width * 0.5f;
            var halfHeight = rect.height * 0.5f;
            var desiredRect = new Rect(halfWidth, halfHeight - boardRect.height, boardRect.width - rect.width, boardRect.height - halfHeight);
            return desiredRect.Contains(innerPoint);
        }
    }
}