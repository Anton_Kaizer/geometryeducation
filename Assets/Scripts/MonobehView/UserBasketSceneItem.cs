using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GeometryEducation.MonobehView
{
    public class UserBasketSceneItem : BaseSceneItem, IDisposable
    {
        private RectTransform _gamePanelTransform;
        private Camera _camera;
        
        private Vector2 _catchPosition;
        private bool _isSelected;
        
        protected override void OnInit(params object[] initalParams)
        {
            _gamePanelTransform = (RectTransform) initalParams[1];
            _camera = (Camera) initalParams[2];
        }

        protected override void OnPointerDowned(PointerEventData eventData)
        {
            _isSelected = true;
            
            _catchPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        }

        private void Update()
        { 
            if(!_isSelected) return;
            
            MoveBasketToPoint(_camera.ScreenToWorldPoint(Input.mousePosition));
        }

        protected override void OnPointerUpped(PointerEventData eventData)
        {
            _isSelected = false;
        }

        private void MoveBasketToPoint(Vector2 toPoint)
        {
            var offsetX = toPoint.x - _catchPosition.x;
            if(Mathf.Approximately(offsetX, 0.0f) || !CheckBounds(toPoint)) return;
            
            _catchPosition = toPoint;
            RectTransform.position += new Vector3(offsetX, 0, 0);
        }

        private bool CheckBounds(Vector2 toPoint)
        {
            var innerPoint = RectTransform.parent.InverseTransformPoint(toPoint);
            var panelRect = _gamePanelTransform.rect;
            var rect = RectTransform.rect;
            
            return innerPoint.x > rect.width &&
                   innerPoint.x < panelRect.width - rect.width / 2;
        }

        public void Dispose()
        {
            _isSelected = false;
            _gamePanelTransform = null;
            _camera = null;
        }
    }
}