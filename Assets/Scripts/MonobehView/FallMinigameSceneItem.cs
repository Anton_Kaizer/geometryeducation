using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Image = UnityEngine.UI.Image;

namespace GeometryEducation.MonobehView
{
    public class FallMinigameSceneItem : BaseSceneItem
    {
        public event Action<FallMinigameSceneItem> ItemCollidedExit;
        public event Action<FallMinigameSceneItem> ItemCollidedBasket;
        
        public Image Image;
        
        [SerializeField] private float _speedModifier;
        private float _speed;
        private float _itemSize;

        protected override void OnInit(params object[] initalParams)
        {
            _speed = (float) initalParams[1];
            _itemSize = (float) initalParams[2];
            Addressables.LoadAssetAsync<Sprite>(initalParams[3]).Completed += SetSpriteOnLoadComplete;
        }

        private void SetSpriteOnLoadComplete(AsyncOperationHandle<Sprite> operation)
        {
            Image.sprite = operation.Result;
            Image.preserveAspect = true;
            RectTransform.sizeDelta = new Vector2(_itemSize, _itemSize);
        }
        
        private void Update()
        {
            var dy = Time.deltaTime * _speed * _speedModifier;
            var position = RectTransform.localPosition;
            RectTransform.localPosition = new Vector3(position.x, position.y - dy, position.z);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Basket"))
            {
                ItemCollidedBasket?.Invoke(this);
            }
            else if(other.gameObject.CompareTag("Finish"))
            {
                ItemCollidedExit?.Invoke(this);
            }
        }
    }
}