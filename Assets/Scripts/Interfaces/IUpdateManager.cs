namespace GeometryEducation.Interfaces
{
    internal interface IUpdateManager : IInitable, IUpdatable
    {
        void Add(IUpdatable updatable);
        void Remove(IUpdatable updatable);
    }
}