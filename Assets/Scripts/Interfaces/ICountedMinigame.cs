namespace GeometryEducation.Interfaces
{
    internal interface ICountedMinigame : IMinigame
    {
        int CurCounter { get; }
    }
}