using System;
using GeometryEducation.MonobehUI;

namespace GeometryEducation.Interfaces
{
    internal interface IMainMenuController : IDisposable
    {
        MainMenuContainer Container { get; }
        void ReturnToMainPage();
    }
}