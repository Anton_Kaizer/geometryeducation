namespace GeometryEducation.Interfaces
{
    internal interface IIdentified
    {
        string Id { get; }
    }
}