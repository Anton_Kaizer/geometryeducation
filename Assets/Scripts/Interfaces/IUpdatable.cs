namespace GeometryEducation.Interfaces
{
    internal interface IUpdatable
    {
        void Update(float dt);
    }
}