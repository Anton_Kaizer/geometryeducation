namespace GeometryEducation.Interfaces
{
    internal interface IJsonReader
    {
        T ReadFromJson<T>(string text);
    }
}