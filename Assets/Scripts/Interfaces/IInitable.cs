namespace GeometryEducation.Interfaces
{
    internal interface IInitable
    {
        void Init(params object[] initalParams);
    }
}