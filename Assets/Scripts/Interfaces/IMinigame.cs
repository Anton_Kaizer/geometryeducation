using System;
using UnityEngine;

namespace GeometryEducation.Interfaces
{
    internal interface IMinigame : IDisposable
    {
        GameObject Root { get; }
        
        void Start();

        void Stop();
    }
}