using System.Collections.Generic;
using GeometryEducation.Interfaces;

namespace GeometryEducation
{
    internal class UpdateManager : IUpdateManager
    {
        private readonly IList<IUpdatable> _itemsToUpdate = new List<IUpdatable>();
        
        public void Init(params object[] initalParams)
        {
        }

        public void Update(float dt)
        {
            foreach (var item in _itemsToUpdate)
            {
                item.Update(dt);
            }
        }

        public void Add(IUpdatable updatable)
        {
            if(_itemsToUpdate.Contains(updatable)) return;

            _itemsToUpdate.Add(updatable);
        }

        public void Remove(IUpdatable updatable)
        {
            if(!_itemsToUpdate.Contains(updatable)) return;
            
            _itemsToUpdate.Remove(updatable);
        }
    }
}